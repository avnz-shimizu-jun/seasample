/**
 *
 */
package jp.co.avnz.sol.service;

/**
 * @author shimizu-jun
 *
 */
public interface SampleService {

	public boolean login(String id, String password);

}
