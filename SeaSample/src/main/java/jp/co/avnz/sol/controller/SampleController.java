/**
 *
 */
package jp.co.avnz.sol.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.avnz.sol.form.LoginForm;
import jp.co.avnz.sol.service.SampleService;

/**
 * @author shimizu-jun
 *
 */
@Controller
public class SampleController {

	/** SampleService */
	@Autowired
	SampleService sampleService;

	/**
	 * http://localhost:8080/init
	 * にアクセスするとこのメソッドが呼ばれる。
	 *
	 * 引数には以下のクラスが使用できる。
	 * ・HttpServletRequest
	 * ・Principal
	 * ・Locale
	 * ・InputStream
	 * ・Reader
	 * ・HttpServletResponse
	 * ・OutputStream
	 * ・Writer
	 * ・HttpSession
	 *
	 * そのほか削除のFormなども引数に指定可能で、@ModelAttribute("hoge")という指定で指定するのが正式なやり方。
	 * このアノテーションは省略可能で、その場合はSpringが自動で割り当ててくれる。
	 * 1個しかFormがないなどの場合は省略でよい。
	 *
	 * 戻り値はString, ModelAndView, voidが一般的。
	 * Stringの場合はViweResolverが名前解決を行う。jspファイルやhtmlファイルの相対パスを返すことが多い。
	 * ModelAndViewはModelとViewを両方返す。
	 * Modelに値を設定すると返却先のhtmlから参照可能。
	 * ViewNameに遷移先を記載する。
	 *
	 * @param response
	 * @return
	 */
	@RequestMapping("/init")
	public ModelAndView init() {
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

		ModelAndView mv = new ModelAndView();
		mv.addObject("nowDate", date.format(formatter));
		mv.setViewName("/login.html");

		return mv;
	}

	/**
	 *
	 * @param form
	 * @return
	 */
	@RequestMapping("/login")
	public String login(LoginForm form, Model model) {
		String id = form.getId();
		String password = form.getPassword();

		if (sampleService.login(id, password)) {
			model.addAttribute("id", id);
			return "/success.html";
		}

		return "/error.html";
	}

}
