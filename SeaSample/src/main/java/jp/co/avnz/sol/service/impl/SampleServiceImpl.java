/**
 *
 */
package jp.co.avnz.sol.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.avnz.sol.model.UserModel;
import jp.co.avnz.sol.repository.User;
import jp.co.avnz.sol.service.SampleService;

/**
 * @author shimizu-jun
 *
 */
@Service
public class SampleServiceImpl implements SampleService {

	@Autowired
	User user;

	/**
	 *
	 */
	@Override
	public boolean login(String id, String password) {
		UserModel userModel = user.selectUser(id, password);
		if (userModel == null) {
			return false;
		}
		return true;
	}

}
