/**
 *
 */
package jp.co.avnz.sol.model;

import lombok.Data;

/**
 * @author shimizu-jun
 *
 */
@Data
public class UserModel {

	private String id;

	private String password;

	private String userName;

	private String age;

	private String gender;

}
