/**
 *
 */
package jp.co.avnz.sol.form;

import lombok.Data;

/**
 * @author shimizu-jun
 *
 */
@Data
public class LoginForm {

	private String id;

	private String password;

}
