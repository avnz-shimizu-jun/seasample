/**
 *
 */
package jp.co.avnz.sol.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.co.avnz.sol.model.UserModel;

/**
 * @author shimizu-jun
 *
 */
@Mapper
public interface User {

	/**
	 * Userテーブルからユーザーを検索する。
	 * @param id ID
	 * @param password パスワード
	 * @return
	 */
	UserModel selectUser(@Param("id") String id, @Param("password") String password);
}
