package jp.co.avnz.sol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeaSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeaSampleApplication.class, args);
	}

}
